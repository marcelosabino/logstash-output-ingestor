# Logstash Output Plugin to Ingestor

![Build
Status](http://build-eu-00.elastic.co/view/LS%20Plugins/view/LS%20Outputs/job/logstash-plugin-output-kafka-unit/badge/icon)

This is a plugin for [Logstash](https://github.com/elastic/logstash), used on [OpenbusBR](https://github.com/Produban/OpenbusBR) to output messages to [Ingestor](https://github.com/Produban/OpenbusBR/tree/model-refactoring/openbus.ingestor)

It is fully free and fully open source. The license is Apache 2.0, meaning you are pretty much free to use it however you want in whatever way.
